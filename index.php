<?php
	error_reporting(0);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>ATM app</title>
		<link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
		<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<!--Created by OSSAI PRECIOUS-->
	<body style="background: url('img/background.jpg'); background-repeat:no-repeat; background-size:100%;">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
				  <a class="navbar-brand" href="#">PeCrio</a>
				</div>
				<div>
					<ul class="nav navbar-nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="#">Contact Us</a></li> 
						<li><a href="#">About Us</a></li> 
						<li><a href="#">Notifications <span class="badge">12</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<form role="form" method="POST" action="check.php">
			<div style="margin-left: 35%; margin-right: 35%; margin-top:5%">
				<div class="container-fluid">
					<div class="jumbotron">
						<div class="text-center">
							<div>
								<img src="img/1k_naira.jpg" style="border-radius:100%; border:2px solid black "; width="120px" height="120px">
							</div><br>
							<div class="form-group">
								<label for="user">User name:</label><br>
								<input type="text" class="form-control" name="user" placeholder="Note: case sensitive" required>
							</div><br>
							<button type="submit" class="btn btn-default">Submit</button>
							<a href="thank_you.php">
								<button type="button" class="btn btn-default">Cancel</button>
							</a><br/><br/>
							&copy PeCrio 2016
						</div>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>