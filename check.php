<?php
	error_reporting(0);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>ATM app</title>
		<link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
		<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<!--Created by OSSAI PRECIOUS-->
	<body style="background: url('img/background.jpg'); background-repeat:no-repeat; background-size:100%;">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
				  <a class="navbar-brand" href="#">PeCrio</a>
				</div>
				<div>
					<ul class="nav navbar-nav">
						<li><a href="index.php">Home</a></li>
						<li><a href="#">Contact Us</a></li> 
						<li><a href="#">About Us</a></li> 
						<li><a href="#">Notifications <span class="badge">12</span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div style="margin-left: 35%; margin-right: 25%">
			<div class="container-fluid">
				<div class="jumbotron">
					<div class="text-center">
					  <div>
					  	<img src="img/1k_naira.jpg" style="border-radius:100%; border:2px solid black "; width="120px" height="120px">
					  </div>
						<h3>
						  	<?php 
						  		$user = $_POST['user'];
								$PeCrio = array('balance' => 7000000000000,
								                'accountype' => 'savings',
								                'gender' => 'male');

								$TosinBot = array('balance' => 6000000000000,
								                'accountype' => 'current',
								                'gender' => 'male');

								$Yemo = array('balance' => 5000000000000,
								                'accountype' => 'domicial',
								                'gender' => 'male');

								$Nanle = array('balance' => 4000000000000,
								                'accountype' => 'savings',
								                'gender' => 'male');

								$Drtalent = array('balance' => 3000000000000,
								                'accountype' => 'current',
								                'gender' => 'male');

								$Jehn = array('balance' => 5500000000000,
								                'accountype' => 'current',
								                'gender' => 'female');
						  		switch ($user) {
						  			case 'PeCrio':
									echo 'Welcome Mr PeCrio' . '<br />' . 'Your ' . $PeCrio['accountype'] . ' account balance is ' . $PeCrio['balance'];
						  				break;
						  			case 'TosinBot':
									echo  'Hello Mr TosinBot' . '<br />' . 'Your ' . $TosinBot['accountype'] . ' account balance is ' . $TosinBot['balance'];
						  				break;
						  			case 'Yemo':
									echo  'Hello Mr Yemo' . '<br />' . 'Your ' . $Yemo['accountype'] . ' account balance is ' . $Yemo['balance'];
						  				break;
						  			case 'Nanle':
									echo  'Hello Mr Nanle' . '<br />' . 'Your ' . $Nanle['accountype'] . ' account balance is ' . $Nanle['balance'];
						  				break;
						  			case 'Drtalent':
									echo  'Hello Dr talent' . '<br />' . 'Your ' . $Drtalent['accountype'] . ' account balance is ' . $Drtalent['balance'];
						  				break;
						  			case 'Jehn':
									echo  'Hello Miss Jehn' . '<br />' . 'Your ' . $Jehn['accountype'] . ' account balance is ' . $Jehn['balance'];
						  				break;
						  			
						  			default:
						  				echo "Sorry no match found, please login again (note: username is case sensitive, or probably you do not currently have an account with us, kindly please sign up";
						  				die();
						  				break;
						  		}
							?><br />
						</h3>
						<div class="form-group">
							<label for="user">Please input the amount you want to withdraw</label>
							<input type="text" class="form-control" name="amount" required>
						</div>
						<a href="thank_you.php">
							<button type="button" class="btn btn-default">Submit</button>
						</a>
						<a href="index.php">
							<button type="button" class="btn btn-default">Cancel</button>
						</a><br /><br />
						&copy PeCrio 2016
					</div>
				</div>
			</div>
		</div>
	</form>
</html>